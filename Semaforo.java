package com.company;
public class Semaforo {
    int timer=200;
    int current_timer=0;
    char colore='R';

    public static final String RED = "\u001B[31m";
    public static final String GREEN = "\u001B[32m";
    public static final String YELLOW = "\u001B[33m";

    public Semaforo(int delay) {
    this.timer=delay;
}
public void start(){
    while (true) {
        if (current_timer < 0) {
            sequenza();
            System.out.println(this);
            current_timer = timer;
        }
        current_timer--;
    }
  }
  public void sequenza(){
    switch (colore){
        case 'R':
            colore='V';
            break;
        case 'V':
            colore='G';
            break;
        case 'G':
            colore='R';
            break;
    }
  }
    public String toString() {
        switch (colore){
            case 'R':
            default:
                return RED+colore;
            case 'V':
                return GREEN+colore;
            case 'G':
                return YELLOW+colore;
        }
    }
}